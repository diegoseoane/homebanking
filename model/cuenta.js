const mongoose = require("mongoose");

const schemaCuenta = new mongoose.Schema({
    nombre: String,
    apellido: String,
    email: String,
    saldo: Number
})

const Cuenta = new mongoose.model("Cuenta", schemaCuenta);

module.exports = {
    Cuenta
}