const mongoose = require("mongoose");
const { Cuenta } = require("../model/cuenta.js");

async function crearCuenta(req, res) {
    try {
        const nuevaCuenta = new Cuenta({ nombre: req.body.nombre, apellido: req.body.apellido, email: req.body.email, saldo: req.body.saldo });
        await nuevaCuenta.save();
        res.status(200).json(`La cuenta de ${nuevaCuenta.nombre} ${nuevaCuenta.apellido} ha sido creada con exito`);
    } catch (error) {
        console.log(error);
    }
}

async function modificarSaldo(req, res) {
    try {
        const c = await Cuenta.findOne({ email: req.body.email }).exec();
        c.saldo += req.body.monto;
        c.save();
        return res.status(200).json(`Su nuevo saldo es ${c.saldo}`);
    } catch (error) {
        console.log(error);
    }
}

async function eliminarCuenta(req, res) {
    try {
        await Cuenta.deleteOne({ email: req.headers.email });
        res.status(200).json(`La cuenta ha sido eliminada`);
    } catch (error) {
        console.log(error);
    }
}

async function realizarTransferencia(req, res) {
    try {
        const db = mongoose.connection;
        const session = await db.startSession();
        session.startTransaction();
        const cuentaOrigen = await Cuenta.findOne({ email: req.body.emailOrigen }).exec();
        const cuentaDestino = await Cuenta.findOne({ email: req.body.emailDestino }).exec();
        if (cuentaOrigen.saldo >= req.body.monto) {
            cuentaOrigen.saldo -= req.body.monto;
            cuentaDestino.saldo += req.body.monto;
            cuentaOrigen.save();
            cuentaDestino.save();
            await session.commitTransaction();
            res.status(200).json(`Transferencia realizada con éxito`);
        } else {
            await session.abortTransaction();
            res.status(400).json(`Monto no suficiente`);
        }
        session.endSession();
    } catch (error) {
        console.log(error)
    }
}

module.exports = {
    crearCuenta,
    modificarSaldo,
    realizarTransferencia,
    eliminarCuenta
}