const express = require("express");
const server = express();
const { initDatabase } = require("./db.js");
const { router } = require("./routers.js");

async function main() {
    await initDatabase();
    server.use("/", router)
    server.listen(3000, () => console.log("servidor 3000 funcionando!"));
}

main();