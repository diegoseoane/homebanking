const express = require("express");
const router = express();
const { crearCuenta, modificarSaldo, realizarTransferencia, eliminarCuenta } = require("./functions.js");

router.use(express.json());
router.post("/cuentas", crearCuenta);
router.put("/cuentas", modificarSaldo);
router.post("/transferencias", realizarTransferencia);
router.delete("/transferencias", eliminarCuenta);

module.exports = {
    router
}